# Mobile Menu Toggle
- This module creates a block with a "Menu" link within it. When clicked that
  link initiates a jQuery slideToggle effect to show or hide a user selected
  menu.

## Created By:
- Kevin Basarab (@kBasarab)
- Kendall Totten (@starryeyez024)

## SETTING UP:
- User must have administer menu permissions to configure.
- Configure module at: /admin/config/user-interface/mobile-menu-toggle
- Insert "Mobile Menu Toggle" block in site where "menu" link should appear.
- Add custom CSS to your theme to hide or show menu by default and style.
